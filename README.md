# README

## .env
Update following variable in .env file to start on your environment
```
URL=(default:pask-dev.uniteam.local)
```
**env file is ignored by git**

## run
To start docker-compose on one host please use command
```
docker-compose -f utils-compose.yml -f main-compose.yml --compatibility up -d
```
two files will be merged by docker-compose during start. On production environment we are using *override files where is configuration for non-local environment with multiple hosts

## mongo
mongo has data mounted to host catalog ./mongo

**mongo directory is ignored by git**

## graylog
- https://$URL/graylog
- admin:admin

## mailhog
Add configuration as a service in a utils-compose.yml for your local test session
```
smtp:
    image: mailhog/mailhog
    restart: always
    container_name: smtp
    ports:
        - 1025:1025
        - 8025:8025
    networks:
        - backend
```

## logging
When app is run locally GELF_ADDRESS should have value udp://127.0.0.1:12201
