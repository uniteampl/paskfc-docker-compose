#!/bin/bash

set -e
set -u

function create_database() {
	local database=$1
	local db_username=$2
	local schema_prefix=$POSTGRES_DEFAULT_SCHEMA_PREFIX
	local profile=$PROFILE
	local tenant="tenant"
	local default_schema="${schema_prefix}${database}"
	echo "create database: '$database'"
	psql -v ON_ERROR_STOP=1 --username "$db_username" <<-EOSQL
	    CREATE DATABASE $database;
	    GRANT ALL PRIVILEGES ON DATABASE $database TO $db_username;
EOSQL
  if [ "$database" = "tenant" ]
  then
    echo "create default schema for database: '$database'"
    psql -v ON_ERROR_STOP=1 --username "$db_username" <<-EOSQL
	    \c $database
	    CREATE SCHEMA IF NOT EXISTS $default_schema;
EOSQL
  elif [ "$profile" != "prod" ]
  then
    echo "create default schema for database: '$database' on '$profile' environment"
    psql -v ON_ERROR_STOP=1 --username "$db_username" <<-EOSQL
	    \c $database
	    CREATE SCHEMA IF NOT EXISTS $POSTGRES_DEFAULT_SCHEMA;
EOSQL
  fi
}

if [ -n "$POSTGRES_MULTIPLE_DATABASES" ]
then
	echo "Multiple database creation requested: $POSTGRES_MULTIPLE_DATABASES"
	for db in $(echo $POSTGRES_MULTIPLE_DATABASES | tr ',' ' '); do
		create_database $db $POSTGRES_USER
		
	done
	echo "Multiple databases created"
fi
